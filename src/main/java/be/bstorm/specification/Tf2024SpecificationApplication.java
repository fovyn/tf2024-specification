package be.bstorm.specification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tf2024SpecificationApplication {

    public static void main(String[] args) {
        SpringApplication.run(Tf2024SpecificationApplication.class, args);
    }

}
