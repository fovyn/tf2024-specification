package be.bstorm.specification.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.Table;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDate;

@Entity(name = "Book")
@Table(name = "t_book")
@EntityListeners(AuditingEntityListener.class)
@Data
public class BookEntity extends BaseEntity {
    private String title;
    private String description;
    private LocalDate outDate;
}
