package be.bstorm.specification.config.jwt.exceptions;

import be.bstorm.specification.config.exceptions.HttpException;
import org.springframework.http.HttpStatus;

import java.util.HashMap;

public class TokenInvalidException extends HttpException {
    public TokenInvalidException() {
        super(HttpStatus.UNAUTHORIZED, new HashMap<>());
    }
}
