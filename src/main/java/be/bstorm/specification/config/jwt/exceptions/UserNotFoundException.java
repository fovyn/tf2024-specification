package be.bstorm.specification.config.jwt.exceptions;

import be.bstorm.specification.config.exceptions.HttpException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.FieldError;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserNotFoundException extends HttpException {

    public UserNotFoundException() {
        super(
                HttpStatus.PRECONDITION_FAILED,
                Map.of(
                        "username",
                        Arrays.asList(
                                new FieldError(
                                        "User",
                                        "username",
                                        "Username doesn't exist"
                                )
                        )
                )
        );
    }
}
