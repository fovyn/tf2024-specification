package be.bstorm.specification.config.jwt.exceptions;

import be.bstorm.specification.config.exceptions.HttpException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TokenExpireException extends HttpException {

    public TokenExpireException() {
        super(HttpStatus.FORBIDDEN, new HashMap<>());
    }
}
