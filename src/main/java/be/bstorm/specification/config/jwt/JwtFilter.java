package be.bstorm.specification.config.jwt;

import be.bstorm.specification.config.jwt.exceptions.TokenExpireException;
import be.bstorm.specification.config.jwt.exceptions.TokenInvalidException;
import be.bstorm.specification.config.jwt.exceptions.UserNotFoundException;
import be.bstorm.specification.utils.jwt.JwtHelper;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Date;

@Component
public class JwtFilter extends OncePerRequestFilter {
    private final JwtHelper jwtHelper;
    private final UserDetailsService userDetailsService;

    public JwtFilter(JwtHelper jwtHelper, UserDetailsService userDetailsService) {
        this.jwtHelper = jwtHelper;
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain
    ) throws ServletException, IOException {
        final String authorization = request.getHeader("Authorization");
        if (authorization != null && authorization.startsWith("Bearer ")) {
            final String token = authorization.replace("Bearer ", "");
            try {
                final String username = jwtHelper.getUsernameFromToken(token);
                final Date expirationDate = jwtHelper.getExpirationDateFromToken(token);

                if (expirationDate.after(new Date())) {
                    UserDetails user = this.userDetailsService.loadUserByUsername(username);
                    if (user == null) {
                        throw new UserNotFoundException();
                    }

                    UsernamePasswordAuthenticationToken uapt = new UsernamePasswordAuthenticationToken(
                            user,
                            null,
                            user.getAuthorities()
                    );
                    uapt.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(uapt); // On est connecté
                }
                else { throw new TokenExpireException(); }
            }catch( Exception e) {
                throw new TokenInvalidException(e);
            }

        }
        filterChain.doFilter(request, response);
    }

}
