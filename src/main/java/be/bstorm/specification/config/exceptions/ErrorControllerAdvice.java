package be.bstorm.specification.config.exceptions;

import be.bstorm.specification.config.exceptions.models.ExceptionDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ErrorControllerAdvice {

    @ExceptionHandler(value = { HttpException.class })
    public ResponseEntity<ExceptionDTO> handleHttpException(HttpException e) {
        return ResponseEntity
                .status(e.getStatus())
                .body(new ExceptionDTO("target", e.getMessages()));
    }
}
