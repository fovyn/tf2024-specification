package be.bstorm.specification.config.exceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import java.util.List;
import java.util.Map;

@Data
public class HttpException extends RuntimeException {
    private HttpStatus status;
    private Map<String, List<FieldError>> messages;

    public HttpException(HttpStatus status, Map<String, List<FieldError>> messages) {
        super();
        this.status = status;
        this.messages = messages;
    }
}
