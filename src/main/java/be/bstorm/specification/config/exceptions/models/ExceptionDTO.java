package be.bstorm.specification.config.exceptions.models;

import org.springframework.validation.FieldError;

import java.util.List;
import java.util.Map;

public record ExceptionDTO(String target, Map<String, List<FieldError>> message) {
}
