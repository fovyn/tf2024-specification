package be.bstorm.specification.repositories;

import be.bstorm.specification.entities.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends
        JpaRepository<BookEntity, Long>,
        JpaSpecificationExecutor<BookEntity>
{
    @Query(value = "SELECT b FROM Book b WHERE b.auditing.active = true")
    List<BookEntity> findAllActive();
}
