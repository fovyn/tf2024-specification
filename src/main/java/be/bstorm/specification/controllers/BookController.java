package be.bstorm.specification.controllers;

import be.bstorm.specification.entities.BookEntity;
import be.bstorm.specification.repositories.BookRepository;
import be.bstorm.specification.utils.request.SearchParam;
import be.bstorm.specification.utils.specifications.SearchSpecification;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = {"/books"})
public class BookController {
    private final BookRepository bookRepository;

    public BookController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @GetMapping(path = {""})
    public ResponseEntity<List<BookEntity>> findAll(
            @RequestParam Map<String, String> params
    ) {
        SearchParam.removeKeys(params, "description");

        List<SearchParam<BookEntity>> searchParams = SearchParam.create(params);

        return ResponseEntity.ok(this.bookRepository.findAll(
                Specification.anyOf(
                        searchParams.stream()
                                .map(SearchSpecification::search)
                                .toList()
                )
        ));
    }
}
