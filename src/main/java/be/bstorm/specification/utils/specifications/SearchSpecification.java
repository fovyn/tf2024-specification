package be.bstorm.specification.utils.specifications;

import be.bstorm.specification.utils.request.SearchParam;
import org.springframework.data.jpa.domain.Specification;

public interface SearchSpecification {

    static <T> Specification<T> search(SearchParam<T> param) {
        return (root, cq, cb) -> switch (param.op()) {
            case EQ -> cb.equal(root.get(param.field()), param.value());
            case NE -> cb.notEqual(root.get(param.field()), param.value());
            case GT -> {
                if (!(param.value() instanceof  Number)) throw new RuntimeException();
                yield cb.gt(root.get(param.field()), (Number) param.value());
            }
            case LT -> {
                if (!(param.value() instanceof  Number)) throw new RuntimeException();
                yield cb.lt(root.get(param.field()), (Number) param.value());
            }
            case GTE -> {
                if (!(param.value() instanceof  Number)) throw new RuntimeException();
                yield cb.ge(root.get(param.field()), (Number) param.value());
            }
            case LTE -> {
                if (!(param.value() instanceof  Number)) throw new RuntimeException();
                yield cb.le(root.get(param.field()), (Number) param.value());
            }
            case START -> cb.like(root.get(param.field()), param.value()+ "%");
            case END ->  cb.like(root.get(param.field()), "%"+ param.value());
            case CONTAINS -> cb.like(root.get(param.field()), "%"+ param.value()+ "%");
            case IN -> root.get(param.field()).in(((String)param.value()).split(","));
            case NIN -> cb.not(root.get(param.field()).in(((String)param.value()).split(",")));
        };
    }
}
