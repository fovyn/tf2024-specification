package be.bstorm.specification.utils.request;

public enum SearchOperator {
    EQ,
    NE,
    GT,
    LT,
    GTE,
    LTE,
    START,
    END,
    CONTAINS,
    IN,
    NIN
}
