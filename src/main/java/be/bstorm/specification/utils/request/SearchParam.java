package be.bstorm.specification.utils.request;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public record SearchParam<T>(
        String field,
        SearchOperator op,
        Object value
) {

    public static <T> SearchParam<T> create(Map.Entry<String, String> entry) {
        String field;
        SearchOperator op;
        Object value;

        String[] key = entry.getKey().split("_");
        if (key.length == 1) {
            op = SearchOperator.EQ;
            field = key[0];
        } else {
            op = SearchOperator.valueOf(key[0].toUpperCase());
            field = key[1];
        }
        value = entry.getValue();

        return new SearchParam(field, op, value);
    }

    public static <T> List<SearchParam<T>> create(Map<String, String> params) {
        return params.entrySet().stream()
                .map(entry -> (SearchParam<T>)SearchParam.create(entry))
                .toList();
    }

    public static void removeKeys(Map<String, String> params, String... keys) {
        Map<String, String> cpy = new HashMap<>(params);
        for(Map.Entry<String, String> entry: cpy.entrySet()) {
            for(String key: keys) {

                if (entry.getKey().equals(key)) {
                    params.remove(key);
                }
                else {
                    for(SearchOperator operator: SearchOperator.values()) {
                        String excludeKey = operator.toString().toLowerCase()+ "_"+ key;

                        if (entry.getKey().equals(excludeKey)) {
                            params.remove(excludeKey);
                        }
                    }
                }
            }
        }
//        params.keySet().forEach((key) -> {
//            Arrays.stream(SearchOperator.values()).forEach((op) -> {
//                for (String exclude: keys) {
//                    if (key.equals(op.toString().toLowerCase()+ "_"+ exclude)) {
//                        params.remove(key);
//                    }
//                    if (key.equals(exclude)) {
//                        params.remove(key);
//                    }
//                }
//            });
//        });
    }
}
